package br.com.itau.pagamento.services;

import br.com.itau.pagamento.models.Cartao;
import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    public Pagamento novoPagamento(Pagamento pagamento, Cartao cartao) {
        if(!cartao.isAtivo()){
            throw new RuntimeException("Cartão inativo. Não é possível utilizá-lo para compras.");
        }
        Pagamento pagamentoDB = pagamentoRepository.save(pagamento);
        return pagamentoDB;
    }

    public List<Pagamento> consultarPagamentos(int cartaoId) {
        List<Pagamento> listaPagamentos = (List) pagamentoRepository.findByCartaoId(cartaoId);
        if(listaPagamentos.size() == 0) {
            throw new RuntimeException("Não há pagamentos a serem faturados para o cartão consultado");
        }
        return listaPagamentos;
    }

    @Transactional
    public void deletarPagamentos(int cartaoId) {
        pagamentoRepository.deleteByCartaoId(cartaoId);
    }
}
