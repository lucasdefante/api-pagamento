package br.com.itau.pagamento.clients;


import br.com.itau.pagamento.models.Cartao;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@FeignClient(name = "CARTAO", configuration = CartaoClientConfiguration.class)
public interface CartaoClient {

    @GetMapping("/cartao/id/{id}")
    Cartao getCartaoById(@PathVariable int id);

    @PostMapping("/cartao/{cartaoId}/pagamento/{pagamentoId}")
    List<Integer> adicionarPagamentoId(@PathVariable int cartaoId, @PathVariable int pagamentoId);
}
