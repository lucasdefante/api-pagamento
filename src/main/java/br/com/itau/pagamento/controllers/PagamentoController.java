package br.com.itau.pagamento.controllers;

import br.com.itau.pagamento.DTOs.CriarPagamentoDTO;
import br.com.itau.pagamento.DTOs.PagamentoMapper;
import br.com.itau.pagamento.clients.CartaoClient;
import br.com.itau.pagamento.models.Cartao;
import br.com.itau.pagamento.models.Pagamento;
import br.com.itau.pagamento.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @Autowired
    private CartaoClient cartaoClient;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public Pagamento novoPagamento(@RequestBody @Valid CriarPagamentoDTO criarPagamentoDTO){
        Pagamento pagamento = pagamentoMapper.toPagamento(criarPagamentoDTO);
        Cartao cartao = cartaoClient.getCartaoById(pagamento.getCartaoId());
        try {
            Pagamento pagamentoDB = pagamentoService.novoPagamento(pagamento, cartao);
            cartaoClient.adicionarPagamentoId(cartao.getId(), pagamentoDB.getId());
            return pagamentoDB;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/pagamentos/{cartaoId}")
    public List<Pagamento> consultarPagamentos(@PathVariable(name = "cartaoId") int cartaoId){
        try {
            return pagamentoService.consultarPagamentos(cartaoId);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/deletar/{cartaoId}")
    public void realizarPagamento(@PathVariable(name = "cartaoId") int cartaoId) {
        pagamentoService.deletarPagamentos(cartaoId);
    }
}
