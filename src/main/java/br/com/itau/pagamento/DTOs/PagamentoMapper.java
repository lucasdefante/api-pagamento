package br.com.itau.pagamento.DTOs;

import br.com.itau.pagamento.models.Pagamento;
import org.springframework.stereotype.Component;

@Component
public class PagamentoMapper {

    public Pagamento toPagamento(CriarPagamentoDTO criarPagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        pagamento.setCartaoId(criarPagamentoDTO.getCartaoId());
        pagamento.setDescricao(criarPagamentoDTO.getDescricao());
        pagamento.setValor(criarPagamentoDTO.getValor());
        return pagamento;
    }

    public CriarPagamentoDTO toCriarPagamentoDTO(Pagamento pagamento) {
        CriarPagamentoDTO criarPagamentoDTO = new CriarPagamentoDTO();
        criarPagamentoDTO.setCartaoId(pagamento.getId());
        criarPagamentoDTO.setDescricao(pagamento.getDescricao());
        criarPagamentoDTO.setValor(pagamento.getValor());
        return criarPagamentoDTO;
    }
}
