package br.com.itau.pagamento.models;

import java.util.List;

public class Cartao {

    private int id;
    private String numero;
    private int clienteId;
    private List<Integer> pagamentosId;
    private List<Integer> faturasId;
    private boolean ativo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }

    public int getClienteId() {
        return clienteId;
    }

    public void setClienteId(int clienteId) {
        this.clienteId = clienteId;
    }

    public List<Integer> getPagamentosId() {
        return pagamentosId;
    }

    public void setPagamentosId(List<Integer> pagamentosId) {
        this.pagamentosId = pagamentosId;
    }

    public List<Integer> getFaturasId() {
        return faturasId;
    }

    public void setFaturasId(List<Integer> faturasId) {
        this.faturasId = faturasId;
    }
}
